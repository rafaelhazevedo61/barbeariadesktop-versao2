Projeto Barberia Desktop - Versão 2 (Usando menu lateral)

Última atualização: 04/08/2021

Olá,

Apresento a vocês o projeto de barbearia para desktop.

Nele são inclusas funcionalidades como:

Cadastrar, alterar, excluir e listar usuários
Cadastrar, alterar, excluir e listar clientes
Cadastrar, alterar, excluir e listar barbeiros
Cadastrar, alterar, excluir e listar serviços
Permissionamento de usuário
Autenticação de usuário
Relatórios
O código é 100% livre para uso, afinal originalmente feito a partir de um canal no youtube chamado WhileTrue. Eu apenas aperfeiçoei o projeto. Espero que façam bom uso do mesmo.

Qualquer dúvida referente ao projeto, crítica ou sugestão. Seguem os meus contatos:

(21) 98656-6485 - Rafael Fonseca Hermes Azevedo rafaelhazevedo61@gmail.com