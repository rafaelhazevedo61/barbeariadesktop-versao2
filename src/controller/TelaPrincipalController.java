package controller;

import com.toedter.calendar.JDateChooser;
import dao.AgendamentosDAO;
import dao.BarbeirosDAO;
import dao.ClientesDAO;
import dao.ServicosDAO;
import dao.UsuariosDAO;
import dao.UsuariosPermissaoDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import model.Agendamentos;
import model.Barbeiros;
import model.Clientes;
import model.Servicos;
import model.Usuarios;
import model.UsuariosPermissao;
import util.Data;
import util.Email;
import view.TelaLogin;
import view.TelaPrincipal;

public class TelaPrincipalController {

    private final TelaPrincipal view;

    private final BarbeirosDAO barbeirosDAO = new BarbeirosDAO();
    private final ClientesDAO clientesDAO = new ClientesDAO();
    private final AgendamentosDAO agendamentosDAO = new AgendamentosDAO();
    private final ServicosDAO servicosDAO = new ServicosDAO();
    private final UsuariosDAO usuariosDAO = new UsuariosDAO();
    private final UsuariosPermissaoDAO usuariosPermissaoDAO = new UsuariosPermissaoDAO();

    //CONSTRUTOR
    public TelaPrincipalController(TelaPrincipal view) {
        this.view = view;
    }

    /*
    PARA BUSCAR PELO CONTROLLER DE CADA TELA
    
    BUSCAR POR:
    
    TELA DE CLIENTES
    TELA DE BARBEIROS
    TELA DE USUARIOS
    TELA DE SERVIÇOS
    TELA DE AGENDAMENTOS
    
     */
    //GERAL
    public void Deslogar() {

        System.setProperty("usuarioLogado", "");

        TelaLogin telaLogin = new TelaLogin();
        telaLogin.setVisible(true);
        view.dispose();

    }

    //TELA DE CLIENTES
    public void PesquisarClientesPorNome() {

        String nomePesquisa = view.getjTextFieldPesquisaClientes().getText().toUpperCase();
        CarregarTabelaClientesPorNome(nomePesquisa);

    }

    public void MouseClickedClientes() {

        JTable tabelaClientes = view.getjTableClientes();

        JTextField nome = view.getjTextFieldNomeCompletoClientes();
        JFormattedTextField cpf = view.getjFormattedTextFieldCPFClientes();
        JTextField email = view.getjTextFieldEmailClientes();
        JFormattedTextField data_nascimento = view.getjFormattedTextFieldDataNascimentoClientes();
        JComboBox sexo = view.getjComboBoxSexoClientes();
        JFormattedTextField cep = view.getjFormattedTextFieldCEPClientes();
        JTextField rua = view.getjTextFieldRuaClientes();
        JTextField numero = view.getjTextFieldNumeroClientes();
        JTextField complemento = view.getjTextFieldComplementoClientes();
        JTextField bairro = view.getjTextFieldBairroClientes();
        JFormattedTextField contato1 = view.getjFormattedTextFieldContato1Clientes();
        JFormattedTextField contato2 = view.getjFormattedTextFieldContato2Clientes();
        JRadioButton recebe_email_sim = view.getjRadioButtonSimClientes();
        JRadioButton recebe_email_nao = view.getjRadioButtonNaoClientes();

        if (tabelaClientes.getSelectedRow() != -1) {

            nome.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 1).toString());
            cpf.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 2).toString());
            email.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 3).toString());
            data_nascimento.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 4).toString());
            sexo.setSelectedIndex((Integer) tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 5));
            cep.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 6).toString());
            rua.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 7).toString());
            numero.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 8).toString());
            complemento.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 9).toString());
            bairro.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 10).toString());
            contato1.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 11).toString());
            contato2.setText(tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 12).toString());

            if ((tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 13).toString()).equals("true")) {

                recebe_email_sim.setSelected(true);
                recebe_email_nao.setSelected(false);

            } else {

                recebe_email_sim.setSelected(false);
                recebe_email_nao.setSelected(true);

            }

        }

    }

    public void ExcluirClientes() {

        JTable tabelaClientes = view.getjTableClientes();

        if (tabelaClientes.getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para excluir um cadastro, é preciso selecionar um registro na tabela");

        } else {

            //RECUPERANDO O ID DO CAMPO SELECIONADO
            int codcliente = ((int) tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 0));

            //CHAMANDO O MÉTODO PARA EXCLUIR O CLIENTE
            clientesDAO.ExcluirCliente(codcliente);

        }

    }

    public void CarregarTabelaClientesPorNome(String clientepesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableClientes().getModel();
        model.setNumRows(0);

        for (Clientes c : clientesDAO.ListarClientesPorNome(clientepesquisa)) {

            model.addRow(new Object[]{
                c.getCodcliente(),
                c.getNome(),
                c.getCpf(),
                c.getEmail(),
                Data.Format(c.getData_nascimento()),
                c.getSexo(),
                c.getCep(),
                c.getRua(),
                c.getNumero(),
                c.getComplemento(),
                c.getBairro(),
                c.getContato1(),
                c.getContato2(),
                c.isRecebe_email()

            });
        }

    }

    public void CarregarTabelaClientes() {

        DefaultTableModel model = (DefaultTableModel) view.getjTableClientes().getModel();
        model.setNumRows(0);

        for (Clientes c : clientesDAO.ListarClientes()) {

            model.addRow(new Object[]{
                c.getCodcliente(),
                c.getNome(),
                c.getCpf(),
                c.getEmail(),
                Data.Format(c.getData_nascimento()),
                c.getSexo(),
                c.getCep(),
                c.getRua(),
                c.getNumero(),
                c.getComplemento(),
                c.getBairro(),
                c.getContato1(),
                c.getContato2(),
                c.isRecebe_email()

            });
        }

    }

    public void AtualizarCliente() {

        try {

            JTable tabelaClientes = view.getjTableClientes();

            /*PREENCHIMENTO OBRIGATORIO*/
            String nome = view.getjTextFieldNomeCompletoClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cpf = view.getjFormattedTextFieldCPFClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String email = view.getjTextFieldEmailClientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String data_nascimento = view.getjFormattedTextFieldDataNascimentoClientes().getText();

            Date dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse(data_nascimento);
            /*PREENCHIMENTO OBRIGATORIO*/
            int sexo = view.getjComboBoxSexoClientes().getSelectedIndex();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cep = view.getjFormattedTextFieldCEPClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String rua = view.getjTextFieldRuaClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String numero = view.getjTextFieldNumeroClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String complemento = view.getjTextFieldComplementoClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String bairro = view.getjTextFieldBairroClientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String contato1 = view.getjFormattedTextFieldContato1Clientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String contato2 = view.getjFormattedTextFieldContato2Clientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            boolean recebe_email;
            if (view.getjRadioButtonSim().isSelected() == true) {

                recebe_email = true;

            } else {

                recebe_email = false;

            }

            //TRATAMENTO DE CAMPOS NULOS
            if (nome.trim().isEmpty() == false) {

                if (!data_nascimento.equals("  /  /    ")) {

//                    if (sexo != 0) {
                    if (!contato1.equals("(  )     -    ")) {

                        //RECUPERANDO O ID DO CAMPO SELECIONADO
                        int codcliente = ((int) tabelaClientes.getValueAt(tabelaClientes.getSelectedRow(), 0));

                        //CHAMANDO O MÉTODO PARA ATUALIZAR O CLIENTE
                        clientesDAO.AtualizarCliente(codcliente, nome, cpf, email, dataNascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email);

                    } else {
                        //contato1
                        JOptionPane.showMessageDialog(null, "Campo de contato1 não pode estar vazio!");

                    }

//                    } else {
//                        //sexo
//                        JOptionPane.showMessageDialog(null, "Campo de sexo não pode estar vazio!");
//
//                    }
                } else {
                    //data_nascimento
                    JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

                }

            } else {
                //nome
                JOptionPane.showMessageDialog(null, "Campo de nome não pode estar vazio!");

            }
        } catch (ParseException ex) {

            Logger.getLogger(TelaPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

        }

    }

    public void CadastrarCliente() {

        try {

            /*PREENCHIMENTO OBRIGATORIO*/
            String nome = view.getjTextFieldNomeCompletoClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cpf = view.getjFormattedTextFieldCPFClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String email = view.getjTextFieldEmailClientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String data_nascimento = view.getjFormattedTextFieldDataNascimentoClientes().getText();

            Date dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse(data_nascimento);
            /*PREENCHIMENTO OBRIGATORIO*/
            int sexo = view.getjComboBoxSexoClientes().getSelectedIndex();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cep = view.getjFormattedTextFieldCEPClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String rua = view.getjTextFieldRuaClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String numero = view.getjTextFieldNumeroClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String complemento = view.getjTextFieldComplementoClientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String bairro = view.getjTextFieldBairroClientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String contato1 = view.getjFormattedTextFieldContato1Clientes().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String contato2 = view.getjFormattedTextFieldContato2Clientes().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            boolean recebe_email;
            if (view.getjRadioButtonSim().isSelected() == true) {

                recebe_email = true;

            } else {

                recebe_email = false;

            }

            //TRATAMENTO DE CAMPOS NULOS
            if (nome.trim().isEmpty() == false) {

                if (!data_nascimento.equals("  /  /    ")) {

//                    if (sexo != 0) {
                    if (!contato1.equals("(  )     -    ")) {

                        //CHAMANDO O MÉTODO PARA CADASTRAR O CLIENTE
                        clientesDAO.CadastrarCliente(nome, cpf, email, dataNascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email);

                    } else {
                        //contato1
                        JOptionPane.showMessageDialog(null, "Campo de contato1 não pode estar vazio!");

                    }

//                    } else {
//                        //sexo
//                        JOptionPane.showMessageDialog(null, "Campo de sexo não pode estar vazio!");
//
//                    }
                } else {
                    //data_nascimento
                    JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

                }

            } else {
                //nome
                JOptionPane.showMessageDialog(null, "Campo de nome não pode estar vazio!");

            }

        } catch (ParseException ex) {

            Logger.getLogger(TelaPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

        }

    }

    public void BotaoPesquisarClientes() {

        PesquisarClientesPorNome();

    }

    public void BotaoExcluirClientes() {

        ExcluirClientes();
        CarregarTabelaClientes();

    }

    public void BotaoSalvarClientes() {

        AtualizarCliente();
        CarregarTabelaClientes();

    }

    public void BotaoNovoClientes() {

        CadastrarCliente();
        CarregarTabelaClientes();

    }

    //TELA DE USUARIOS
    public void PesquisarUsuariosPorUsuario() {

        String usuarioPesquisa = view.getjTextFieldPesquisaUsuarios().getText().toUpperCase();
        CarregarTabelaUsuariosPorUsuario(usuarioPesquisa);

    }

    public void MouseClickedUsuarios() {

        JTable tabelaUsuarios = view.getjTableUsuarios();

        JTextField usuario = view.getjTextFieldUsuario();
        JPasswordField senha = view.getjPasswordFieldSenha();
//        JComboBox permissao = view.getjComboBoxPermissao();

        if (tabelaUsuarios.getSelectedRow() != -1) {

            usuario.setText(tabelaUsuarios.getValueAt(tabelaUsuarios.getSelectedRow(), 1).toString());
            senha.setText(tabelaUsuarios.getValueAt(tabelaUsuarios.getSelectedRow(), 2).toString());
//            permissao.setSelectedIndex((Integer) tabelaUsuarios.getValueAt(tabelaUsuarios.getSelectedRow(), 3));

        }

    }

    public void CarregarJComboBoxUsuarios() {

        for (UsuariosPermissao usp : usuariosPermissaoDAO.ListarPermissoesJComboBox()) {
            view.getjComboBoxPermissao().addItem(usp);
        }

    }

    public void CarregarTabelaUsuarios() {

        DefaultTableModel model = (DefaultTableModel) view.getjTableUsuarios().getModel();
        model.setNumRows(0);

        for (Usuarios c : usuariosDAO.ListarUsuariosComInnerJoin()) {

            model.addRow(new Object[]{
                c.getCodusuario(),
                c.getUsuario(),
                c.getSenha(),
                /*INNER JOIN*/ c.getPermissaoString()

            });
        }

    }

    public void CarregarTabelaUsuariosPorUsuario(String usuariopesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableUsuarios().getModel();
        model.setNumRows(0);

        for (Usuarios c : usuariosDAO.ListarUsuariosPorUsuarioComInnerJoin(usuariopesquisa)) {

            model.addRow(new Object[]{
                c.getCodusuario(),
                c.getUsuario(),
                c.getSenha(),
                /*INNER JOIN*/ c.getPermissaoString()
            });
        }
    }

    public void ExcluirUsuario() {

        JTable tabelaUsuarios = view.getjTableUsuarios();

        if (tabelaUsuarios.getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para excluir um cadastro, é preciso selecionar um registro na tabela");

        } else {

            //RECUPERANDO O ID DO CAMPO SELECIONADO
            int codusuario = ((int) tabelaUsuarios.getValueAt(tabelaUsuarios.getSelectedRow(), 0));

            //CHAMANDO O MÉTODO PARA EXCLUIR O USUÁRIO
            usuariosDAO.ExcluirUsuario(codusuario);
        }

    }

    public void AtualizarUsuario() {

        JTable tabelaUsuarios = view.getjTableUsuarios();

        /*PREENCHIMENTO OBRIGATORIO*/
        String usuario = view.getjTextFieldUsuario().getText();
        /*PREENCHIMENTO OBRIGATORIO*/
        String senha = view.getjPasswordFieldSenha().getText();
        /*PREENCHIMENTO OBRIGATORIO*/
        int permissao = view.getjComboBoxPermissao().getSelectedIndex();

        if (tabelaUsuarios.getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para alterar um cadastro, é preciso selecionar um registro na tabela");

        } else {

            //TRATAMENTO DE CAMPOS NULOS
            if (usuario.trim().isEmpty() == false) {

                if (senha.trim().isEmpty() == false) {

                    if (permissao != 0) {

                        //RECUPERANDO O ID DO CAMPO SELECIONADO
                        int codusuario = ((int) tabelaUsuarios.getValueAt(tabelaUsuarios.getSelectedRow(), 0));

                        //CHAMANDO O MÉTODO PARA ATUALIZAR O USUÁRIO
                        usuariosDAO.AtualizarUsuario(codusuario, usuario, senha, permissao);

                    } else {
                        //permissao
                        JOptionPane.showMessageDialog(null, "Campo de permissao não pode estar vazio!");
                    }

                } else {
                    //senha
                    JOptionPane.showMessageDialog(null, "Campo de senha não pode estar vazio!");
                }

            } else {
                //usuario
                JOptionPane.showMessageDialog(null, "Campo de usuario não pode estar vazio!");

            }

        }

    }

    public void CadastrarUsuario() {

        /*PREENCHIMENTO OBRIGATORIO*/
        String usuario = view.getjTextFieldUsuario().getText().toUpperCase();
        /*PREENCHIMENTO OBRIGATORIO*/
        String senha = view.getjPasswordFieldSenha().getText();
        /*PREENCHIMENTO OBRIGATORIO*/
        String permissao = view.getjComboBoxPermissao().getSelectedItem().toString();
        int permissaoInteiro = view.getjComboBoxPermissao().getSelectedIndex();

        //TRATAMENTO DE CAMPOS NULOS
        if (usuario.trim().isEmpty() == false) {

            if (senha.trim().isEmpty() == false) {

                if (permissaoInteiro != 0) {

                    //RETORNAR CODPERMISSAO DO USUARIO A SER CADASTRADO
                    int codpermissao = usuariosPermissaoDAO.RetornarCodPermissaoPorPermissao(permissao);

                    //CHAMANDO O MÉTODO PARA CADASTRAR O USUÁRIO
                    usuariosDAO.CadastrarUsuario(usuario, senha, codpermissao);

                } else {
                    //permissao
                    JOptionPane.showMessageDialog(null, "Campo de permissao não pode estar vazio!");
                }

            } else {
                //senha
                JOptionPane.showMessageDialog(null, "Campo de senha não pode estar vazio!");
            }

        } else {
            //usuario
            JOptionPane.showMessageDialog(null, "Campo de usuario não pode estar vazio!");

        }

    }

    public void LimparCamposUsuarios() {

        view.getjTextFieldUsuario().setText("");
        view.getjPasswordFieldSenha().setText("");
        view.getjComboBoxPermissao().setSelectedIndex(0);

    }

    public void BotaoPesquisarUsuarios() {

        PesquisarUsuariosPorUsuario();

    }

    public void BotaoLimparCamposUsuarios() {

        LimparCamposUsuarios();

    }

    public void BotaoExcluirUsuarios() {

        ExcluirUsuario();
        CarregarTabelaUsuarios();
        LimparCamposUsuarios();

    }

    public void BotaoSalvarUsuarios() {

        AtualizarUsuario();
        CarregarTabelaUsuarios();
        LimparCamposUsuarios();

    }

    public void BotaoNovoUsuarios() {

        CadastrarUsuario();
        CarregarTabelaUsuarios();
        LimparCamposUsuarios();

    }

    //TELA DE SERVIÇOS
    public void LimparJComboBoxServicos() {

        view.getjComboBoxServico().removeAllItems();

    }

    public void CarregarJComboBoxServicos() {

        for (Servicos sv : servicosDAO.ListarServicosJComboBox()) {
            view.getjComboBoxServico().addItem(sv);
        }

    }

    public void PesquisarServicoPorServico() {

        String servicoPesquisa = view.getjTextFieldPesquisaServicos().getText().toUpperCase();
        CarregarTabelaServicosPorServico(servicoPesquisa);

    }

    public void MouseClickedServicos() {

        JTable tabelaServicos = view.getjTableServicos();

        JTextField nomeservico = view.getjTextFieldServicos();
        JFormattedTextField valor = view.getjFormattedTextFieldValorServicos();
        JTextArea observacao = view.getjTextAreaObservacaoServicos();

        if (tabelaServicos.getSelectedRow() != -1) {

            nomeservico.setText(tabelaServicos.getValueAt(tabelaServicos.getSelectedRow(), 1).toString());
            valor.setText(tabelaServicos.getValueAt(tabelaServicos.getSelectedRow(), 2).toString());
            observacao.setText(tabelaServicos.getValueAt(tabelaServicos.getSelectedRow(), 3).toString());

        }

    }

    public void AtualizarServico() {

        String valorString = view.getjFormattedTextFieldValorServicos().getText();

        if (valorString.trim().isEmpty() == true) {

            //valor
            JOptionPane.showMessageDialog(null, "Campo de valor tabelado não pode estar vazio!");

        } else {

            JTable tabelaServicos = view.getjTableServicos();

            /*PREENCHIMENTO OBRIGATORIO*/
            String nomeservico = view.getjTextFieldServicos().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            double valor = Double.parseDouble(valorString);
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String observacao = view.getjTextAreaObservacaoServicos().getText();

            if (tabelaServicos.getSelectedRow() == -1) {

                JOptionPane.showMessageDialog(null, "Para alterar um cadastro, é preciso selecionar um registro na tabela");

            } else {

                //TRATAMENTO DE CAMPOS NULOS
                if (nomeservico.trim().isEmpty() == false) {

                    //RECUPERANDO O ID DO CAMPO SELECIONADO
                    int codservico = ((int) tabelaServicos.getValueAt(tabelaServicos.getSelectedRow(), 0));

                    //PASSANDO O CONSTRUTOR DA CLASSE MODELO COMO PARÂMETRO PARA O MÉTODO NA CLASSE DAO
                    servicosDAO.AtualizarServico(codservico, nomeservico, valor, observacao);

                } else {
                    //nomeservico
                    JOptionPane.showMessageDialog(null, "Campo de nome serviço não pode estar vazio!");

                }

            }

        }

    }

    public void ExcluirServico() {

        JTable tabelaServicos = view.getjTableServicos();

        if (tabelaServicos.getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para excluir um cadastro, é preciso selecionar um registro na tabela");

        } else {

            int codservico = ((int) tabelaServicos.getValueAt(tabelaServicos.getSelectedRow(), 0));

            servicosDAO.ExcluirServico(codservico);

        }

    }

    public void CadastrarServico() {

        String valorString = view.getjFormattedTextFieldValorServicos().getText();

        if (valorString.trim().isEmpty() == true) {

            //valor
            JOptionPane.showMessageDialog(null, "Campo de valor tabelado não pode estar vazio!");

        } else {

            /*PREENCHIMENTO OBRIGATORIO*/
            String nomeservico = view.getjTextFieldServicos().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            double valor = Double.parseDouble(valorString);
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String observacao = view.getjTextAreaObservacao().getText();

            //TRATAMENTO DE CAMPOS NULOS
            if (nomeservico.trim().isEmpty() == false) {

                //PASSANDO O CONSTRUTOR DA CLASSE MODELO COMO PARÂMETRO PARA O MÉTODO NA CLASSE DAO
                servicosDAO.CadastrarServico(nomeservico, valor, observacao);

            } else {
                //nomeservico
                JOptionPane.showMessageDialog(null, "Campo de nome serviço não pode estar vazio!");

            }

        }
    }

    public void CarregarTabelaServicos() {

        DefaultTableModel model = (DefaultTableModel) view.getjTableServicos().getModel();
        model.setNumRows(0);

        ServicosDAO dao = new ServicosDAO();

        for (Servicos s : dao.ListarServicos()) {

            model.addRow(new Object[]{
                s.getCodservico(),
                s.getNome(),
                s.getValor(),
                s.getObservacao()

            });
        }
    }

    public void CarregarTabelaServicosPorServico(String servicopesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableServicos().getModel();
        model.setNumRows(0);

        for (Servicos s : servicosDAO.ListarServicosPorServico(servicopesquisa)) {

            model.addRow(new Object[]{
                s.getCodservico(),
                s.getNome(),
                s.getValor(),
                s.getObservacao()

            });
        }
    }

    public void LimparCamposServicos() {

        view.getjTextFieldServicos().setText("");
        view.getjFormattedTextFieldValorServicos().setText("");
        view.getjTextAreaObservacaoServicos().setText("");

    }

    public void BotaoLimparServicos() {

        LimparCamposServicos();

    }

    public void BotaoPesquisarServicos() {

        PesquisarServicoPorServico();

    }

    public void BotaoSalvarServicos() {

        AtualizarServico();
        CarregarTabelaServicos();
        LimparJComboBoxServicos();
        CarregarJComboBoxServicos();

    }

    public void BotaoExcluirServicos() {

        ExcluirServico();
        CarregarTabelaServicos();
        LimparJComboBoxServicos();
        CarregarJComboBoxServicos();

    }

    public void BotaoNovoServicos() {

        CadastrarServico();
        CarregarTabelaServicos();
        LimparJComboBoxServicos();
        CarregarJComboBoxServicos();

    }

    //TELA DE BARBEIROS
    public void LimparJComboBoxBarbeiros() {

        view.getjComboBoxBarbeiro().removeAllItems();

    }

    public void CarregarJComboBoxBarbeiros() {

        for (Barbeiros bb : barbeirosDAO.ListarBarbeirosJComboBox()) {
            view.getjComboBoxBarbeiro().addItem(bb);
        }

    }

    public void PesquisarBarbeirosPorNome() {

        String nomePesquisa = view.getjTextFieldPesquisaBarbeiros().getText().toUpperCase();
        CarregarTabelaBarbeirosPorNome(nomePesquisa);

    }

    public void MouseClickedBarbeiros() {

        JTable tabelaBarbeiros = view.getjTableBarbeiros();

        JTextField nome = view.getjTextFieldNomeCompleto();
        JFormattedTextField cpf = view.getjFormattedTextFieldCPF();
        JTextField email = view.getjTextFieldEmail();
        JFormattedTextField data_nascimento = view.getjFormattedTextFieldDataNascimento();
        JComboBox sexo = view.getjComboBoxSexo();
        JFormattedTextField cep = view.getjFormattedTextFieldCEP();
        JTextField rua = view.getjTextFieldRua();
        JTextField numero = view.getjTextFieldNumero();
        JTextField complemento = view.getjTextFieldComplemento();
        JTextField bairro = view.getjTextFieldBairro();
        JFormattedTextField contato1 = view.getjFormattedTextFieldContato1();
        JFormattedTextField contato2 = view.getjFormattedTextFieldContato2();
        JRadioButton recebe_email_sim = view.getjRadioButtonSim();
        JRadioButton recebe_email_nao = view.getjRadioButtonNao();

        if (tabelaBarbeiros.getSelectedRow() != -1) {

            nome.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 1).toString());
            cpf.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 2).toString());
            email.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 3).toString());
            data_nascimento.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 4).toString());
            sexo.setSelectedIndex((Integer) tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 5));
            cep.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 6).toString());
            rua.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 7).toString());
            numero.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 8).toString());
            complemento.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 9).toString());
            bairro.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 10).toString());
            contato1.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 11).toString());
            contato2.setText(tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 12).toString());

            if ((tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 13).toString()).equals("true")) {

                recebe_email_sim.setSelected(true);
                recebe_email_nao.setSelected(false);

            } else {

                recebe_email_sim.setSelected(false);
                recebe_email_nao.setSelected(true);

            }

        }

    }

    public void ExcluirBarbeiro() {

        JTable tabelaBarbeiros = view.getjTableBarbeiros();

        if (tabelaBarbeiros.getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para excluir um cadastro, é preciso selecionar um registro na tabela");

        } else {

            int codbarbeiro = ((int) tabelaBarbeiros.getValueAt(tabelaBarbeiros.getSelectedRow(), 0));

            barbeirosDAO.ExcluirBarbeiro(codbarbeiro);

        }

    }

    public void AtualizarBarbeiro() {

        try {

            /*PREENCHIMENTO OBRIGATORIO*/
            String nome = view.getjTextFieldNomeCompleto().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cpf = view.getjFormattedTextFieldCPF().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String email = view.getjTextFieldEmail().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String data_nascimento = view.getjFormattedTextFieldDataNascimento().getText();

            Date dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse(data_nascimento);

            /*PREENCHIMENTO OBRIGATORIO*/
            int sexo = view.getjComboBoxSexo().getSelectedIndex();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cep = view.getjFormattedTextFieldCEP().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String rua = view.getjTextFieldRua().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String numero = view.getjTextFieldNumero().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String complemento = view.getjTextFieldComplemento().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String bairro = view.getjTextFieldBairro().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String contato1 = view.getjFormattedTextFieldContato1().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String contato2 = view.getjFormattedTextFieldContato2().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            boolean recebe_email;

            if (view.getjRadioButtonSim().isSelected() == true) {

                recebe_email = true;

            } else {

                recebe_email = false;

            }

            if (view.getjTableBarbeiros().getSelectedRow() == -1) {

                JOptionPane.showMessageDialog(null, "Para alterar um cadastro, é preciso selecionar um registro na tabela");

            } else {

                //TRATAMENTO DE CAMPOS NULOS
                if (nome.trim().isEmpty() == false) {

                    if (!data_nascimento.equals("  /  /    ")) {

//                        if (sexo != 0) {
                        if (!contato1.equals("(  )     -    ")) {

                            //RECUPERANDO O ID DO CAMPO SELECIONADO
                            int codbarbeiro = ((int) view.getjTableBarbeiros().getValueAt(view.getjTableBarbeiros().getSelectedRow(), 0));

                            //PASSANDO O CONSTRUTOR DA CLASSE MODELO COMO PARÂMETRO PARA O MÉTODO NA CLASSE DAO
                            barbeirosDAO.AtualizarBarbeiro(codbarbeiro, nome, cpf, email, sexo, dataNascimento, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email);
                        } else {
                            //contato1
                            JOptionPane.showMessageDialog(null, "Campo de contato1 não pode estar vazio!");

                        }

//                        } else {
//                            //sexo
//                            JOptionPane.showMessageDialog(null, "Campo de sexo não pode estar vazio!");
//
//                        }
                    } else {
                        //data_nascimento
                        JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

                    }

                } else {
                    //nome
                    JOptionPane.showMessageDialog(null, "Campo de nome não pode estar vazio!");

                }
            }

        } catch (ParseException ex) {

            Logger.getLogger(TelaPrincipalController.class
                    .getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void CadastrarBarbeiro() {

        try {
            /*PREENCHIMENTO OBRIGATORIO*/
            String nome = view.getjTextFieldNomeCompleto().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cpf = view.getjFormattedTextFieldCPF().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String email = view.getjTextFieldEmail().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String data_nascimento = view.getjFormattedTextFieldDataNascimento().getText();

            Date dataNascimento = new SimpleDateFormat("dd/MM/yyyy").parse(data_nascimento);

            /*PREENCHIMENTO OBRIGATORIO*/
            int sexo = view.getjComboBoxSexo().getSelectedIndex();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String cep = view.getjFormattedTextFieldCEP().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String rua = view.getjTextFieldRua().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String numero = view.getjTextFieldNumero().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String complemento = view.getjTextFieldComplemento().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String bairro = view.getjTextFieldBairro().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            String contato1 = view.getjFormattedTextFieldContato1().getText();
            /*PREENCHIMENTO NÃO OBRIGATORIO*/
            String contato2 = view.getjFormattedTextFieldContato2().getText();
            /*PREENCHIMENTO OBRIGATORIO*/
            boolean recebe_email;

            if (view.getjRadioButtonSim().isSelected() == true) {

                recebe_email = true;

            } else {

                recebe_email = false;

            }

            //TRATAMENTO DE CAMPOS NULOS
            if (nome.trim().isEmpty() == false) {

                if (!data_nascimento.equals("  /  /    ")) {

//                    if (sexo != 0) {
                    if (!contato1.equals("(  )     -    ")) {

                        barbeirosDAO.CadastrarBarbeiro(nome, cpf, email, sexo, dataNascimento, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email);

                    } else {
                        //contato1
                        JOptionPane.showMessageDialog(null, "Campo de contato1 não pode estar vazio!");

                    }

//                    } else {
//                        //sexo
//                        JOptionPane.showMessageDialog(null, "Campo de sexo não pode estar vazio!");
//
//                    }
                } else {
                    //data_nascimento
                    JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");

                }

            } else {
                //nome
                JOptionPane.showMessageDialog(null, "Campo de nome não pode estar vazio!");

            }

        } catch (ParseException ex) {
            Logger.getLogger(TelaPrincipalController.class
                    .getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Campo de data nascimento não pode estar vazio!");
        }

    }

    public void CarregarTabelaBarbeiros() {

        DefaultTableModel model = (DefaultTableModel) view.getjTableBarbeiros().getModel();
        model.setNumRows(0);

        for (Barbeiros b : barbeirosDAO.ListarBarbeiros()) {

            model.addRow(new Object[]{
                b.getCodbarbeiro(),
                b.getNome(),
                b.getCpf(),
                b.getEmail(),
                Data.Format(b.getData_nascimento()),
                b.getSexo(),
                b.getCep(),
                b.getRua(),
                b.getNumero(),
                b.getComplemento(),
                b.getBairro(),
                b.getContato1(),
                b.getContato2(),
                b.isRecebe_email()

            });
        }
    }

    public void CarregarTabelaBarbeirosPorNome(String nomepesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableBarbeiros().getModel();
        model.setNumRows(0);

        for (Barbeiros b : barbeirosDAO.ListarBarbeirosPorNome(nomepesquisa)) {

            model.addRow(new Object[]{
                b.getCodbarbeiro(),
                b.getNome(),
                b.getCpf(),
                b.getEmail(),
                Data.Format(b.getData_nascimento()),
                b.getSexo(),
                b.getCep(),
                b.getRua(),
                b.getNumero(),
                b.getComplemento(),
                b.getBairro(),
                b.getContato1(),
                b.getContato2(),
                b.isRecebe_email()

            });
        }
    }

    public void LimparCamposBarbeiros() {

        view.getjTextFieldNomeCompleto().setText("");
        view.getjFormattedTextFieldCPF().setText("");
        view.getjTextFieldEmail().setText("");
        view.getjFormattedTextFieldDataNascimento().setText("");
        //não zera jcombobox
        //não zera jradiobutton
        view.getjFormattedTextFieldCEP().setText("");
        view.getjTextFieldRua().setText("");
        view.getjTextFieldNumero().setText("");
        view.getjTextFieldComplemento().setText("");
        view.getjTextFieldBairro().setText("");
        view.getjFormattedTextFieldContato1().setText("");
        view.getjFormattedTextFieldContato2().setText("");

    }

    public void BotaoPesquisarBarbeiros() {

        PesquisarBarbeirosPorNome();

    }

    public void BotaoLimparBarbeiros() {

        LimparCamposBarbeiros();

    }

    public void BotaoExcluirBarbeiros() {

        ExcluirBarbeiro();
        CarregarTabelaBarbeiros();
        LimparJComboBoxBarbeiros();
        CarregarJComboBoxBarbeiros();

    }

    public void BotaoSalvarBarbeiros() {

        AtualizarBarbeiro();
        CarregarTabelaBarbeiros();
        LimparJComboBoxBarbeiros();
        CarregarJComboBoxBarbeiros();

    }

    public void BotaoNovoBarbeiros() {

        CadastrarBarbeiro();
        CarregarTabelaBarbeiros();
        LimparJComboBoxBarbeiros();
        CarregarJComboBoxBarbeiros();

    }

    //TELA DE AGENDAMENTOS
    public void PesquisarAgendamentoPorServico() {

        String servicoPesquisa = view.getjTextFieldPesquisa().getText().toUpperCase();
        CarregarTabelaAgendamentosPorServico(servicoPesquisa);

    }

    public void PesquisarAgendamentoPorBarbeiro() {

        String barbeiroPesquisa = view.getjTextFieldPesquisa().getText().toUpperCase();
        CarregarTabelaAgendamentosPorBarbeiro(barbeiroPesquisa);

    }

    public void PesquisarAgendamentoPorCliente() {

        String clientePesquisa = view.getjTextFieldPesquisa().getText().toUpperCase();
        CarregarTabelaAgendamentosPorCliente(clientePesquisa);

    }

    public void MouseClickedAgendamentos() {

        JTable tabelaAgendamentos = view.getjTableAgendamentos();

//        JComboBox cliente = view.getjComboBoxCliente();
//        JComboBox barbeiro = view.getjComboBoxBarbeiro();
//        JComboBox servico = view.getjComboBoxServico();
        JTextField valor = view.getjTextFieldValor();
        JDateChooser data = view.getjDateChooserAgendamentos();
        JFormattedTextField hora = view.getjFormattedTextFieldHora();
        JTextArea observacao = view.getjTextAreaObservacao();

        if (tabelaAgendamentos.getSelectedRow() != -2) {

            try {

                //ARRUMANDO O RETORNO DA DATA
                String retornoData = tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 5).toString();
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date date = formatter.parse(retornoData);

//                cliente.setSelectedItem(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 1).toString());
//                barbeiro.setSelectedItem(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 2).toString());
//                servico.setSelectedItem(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 3).toString());
                valor.setText(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 4).toString());
                data.setDate(date);
                hora.setText(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 6).toString());
                observacao.setText(tabelaAgendamentos.getValueAt(tabelaAgendamentos.getSelectedRow(), 7).toString());

            } catch (ParseException ex) {

                Logger.getLogger(TelaPrincipalController.class
                        .getName()).log(Level.SEVERE, null, ex);

            }

        }

    }

    public void JComboBoxServiçosAgendamento() {

        //RETORNAR VALOR POR SERVIÇO EM UM JTEXTFIELD
        String servico = view.getjComboBoxServico().getSelectedItem().toString();
        double valor = servicosDAO.RetornarValorPorServico(servico);
        String valorString = Double.toString(valor);

        view.getjTextFieldValor().setText(valorString);

    }

    public void ConcluirAgendamento() {

        if (view.getjTableAgendamentos().getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para concluir um agendamento, é preciso selecionar um registro na tabela");

        } else {

            //RECUPERANDO O ID DO AGENDAMENTO SELECIONADO
            int codagendamento = ((int) view.getjTableAgendamentos().getValueAt(view.getjTableAgendamentos().getSelectedRow(), 0));

            //EXCLUINDO AGENDAMENTO
            agendamentosDAO.ConcluirAgendamento(codagendamento);
            CarregarTabelaAgendamentos();

        }

    }

    public void ExcluirAgendamento() {

        if (view.getjTableAgendamentos().getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para excluir um agendamento, é preciso selecionar um registro na tabela");

        } else {

            //RECUPERANDO O ID DO AGENDAMENTO SELECIONADO
            int codagendamento = ((int) view.getjTableAgendamentos().getValueAt(view.getjTableAgendamentos().getSelectedRow(), 0));

            //EXCLUINDO AGENDAMENTO
            agendamentosDAO.ExcluirAgendamento(codagendamento);

        }

    }

    public void AtualizarAgendamento() {

        /*PREENCHIMENTO OBRIGATORIO*/
        String valorString = view.getjTextFieldValor().getText();

        if (valorString.trim().isEmpty() == true) {

            JOptionPane.showMessageDialog(null, "Campo de valor não pode estar vazio!");

        }

        /*PREENCHIMENTO OBRIGATORIO*/
        int clientes = view.getjComboBoxCliente().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        int barbeiros = view.getjComboBoxBarbeiro().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        int servicos = view.getjComboBoxServico().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        double valor = Double.parseDouble(view.getjTextFieldValor().getText());
        /*PREENCHIMENTO OBRIGATORIO*/
        String hora = view.getjFormattedTextFieldHora().getText();
        /*PREENCHIMENTO NÃO OBRIGATORIO*/
        String observacao = view.getjTextAreaObservacao().getText();
        /*PREENCHIMENTO OBRIGATORIO*/
        Date data = view.getjDateChooserAgendamentos().getDate();

        if (view.getjTableAgendamentos().getSelectedRow() == -1) {

            JOptionPane.showMessageDialog(null, "Para alterar um cadastro, é preciso selecionar um registro na tabela");

        } else {

            //TRATAMENTO DE CAMPOS NULOS
            if (clientes != 0) {

                if (barbeiros != 0) {

                    if (servicos != 0) {

                        if (data != null) {

                            if (!hora.equals("  :  ")) {

                                //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO CLIENTE
                                String clienteString = view.getjComboBoxCliente().getSelectedItem().toString();
                                int codcliente = clientesDAO.RetornaCodclientePorClientes(clienteString);
                                System.out.println("Codcliente é: " + codcliente);

                                //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO BARBEIRO
                                String barbeiroString = view.getjComboBoxBarbeiro().getSelectedItem().toString();
                                int codbarbeiro = barbeirosDAO.RetornaCodbarbeiroPorBarbeiros(barbeiroString);
                                System.out.println("Codbarbeiro é: " + codbarbeiro);

                                //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO SERVIÇO
                                String servicoString = view.getjComboBoxServico().getSelectedItem().toString();
                                int codservico = servicosDAO.RetornaCodservicoPorServicos(servicoString);
                                System.out.println("Codservico é: " + codservico);

                                //RECUPERANDO O ID DO AGENDAMENTO SELECIONADO
                                int codagendamento = ((int) view.getjTableAgendamentos().getValueAt(view.getjTableAgendamentos().getSelectedRow(), 0));

                                //ATUALIZANDO O AGENDAMENTO
                                agendamentosDAO.AtualizarAgendamento(codagendamento, codcliente, codbarbeiro, codservico, valor, data, hora, observacao);

                            } else {
                                //hora
                                JOptionPane.showMessageDialog(null, "Campo de hora não pode estar vazio!");

                            }

                        } else {
                            //data
                            JOptionPane.showMessageDialog(null, "Campo de data não pode estar vazio!");

                        }

                    } else {
                        //servicos
                        JOptionPane.showMessageDialog(null, "Campo de serviços não pode estar vazio!");

                    }

                } else {
                    //barbeiros
                    JOptionPane.showMessageDialog(null, "Campo de barbeiros não pode estar vazio!");

                }

            } else {
                //clientes
                JOptionPane.showMessageDialog(null, "Campo de clientes não pode estar vazio!");

            }

        }

    }

    public void CadastrarAgendamento() {

        /*PREENCHIMENTO OBRIGATORIO*/
        String valorString = view.getjTextFieldValor().getText();

        if (valorString.trim().isEmpty() == true) {

            JOptionPane.showMessageDialog(null, "Campo de valor não pode estar vazio!");

        }

        /*PREENCHIMENTO OBRIGATORIO*/
        int clientes = view.getjComboBoxCliente().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        int barbeiros = view.getjComboBoxBarbeiro().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        int servicos = view.getjComboBoxServico().getSelectedIndex();
        /*PREENCHIMENTO OBRIGATORIO*/
        double valor = Double.parseDouble(view.getjTextFieldValor().getText());
        /*PREENCHIMENTO OBRIGATORIO*/
        String hora = view.getjFormattedTextFieldHora().getText();
        /*PREENCHIMENTO NÃO OBRIGATORIO*/
        String observacao = view.getjTextAreaObservacao().getText();
        /*PREENCHIMENTO OBRIGATORIO*/
        Date data = view.getjDateChooserAgendamentos().getDate();

        String dataConvertida = Data.Format(data);

        //TRATAMENTO DE CAMPOS NULOS
        if (clientes != 0) {

            if (barbeiros != 0) {

                if (servicos != 0) {

                    if (data != null) {

                        if (!hora.equals("  :  ")) {

                            //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO CLIENTE
                            String clienteString = view.getjComboBoxCliente().getSelectedItem().toString();
                            int codcliente = clientesDAO.RetornaCodclientePorClientes(clienteString);
                            System.out.println("Codcliente é: " + codcliente);

                            //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO BARBEIRO
                            String barbeiroString = view.getjComboBoxBarbeiro().getSelectedItem().toString();
                            int codbarbeiro = barbeirosDAO.RetornaCodbarbeiroPorBarbeiros(barbeiroString);
                            System.out.println("Codbarbeiro é: " + codbarbeiro);

                            //PASSANDO O TEXTO PARA O MÉTODO EM DAO PARA RETORNAR O ID DO SERVIÇO
                            String servicoString = view.getjComboBoxServico().getSelectedItem().toString();
                            int codservico = servicosDAO.RetornaCodservicoPorServicos(servicoString);
                            System.out.println("Codservico é: " + codservico);

                            //CADASTRANDO O AGENDAMENTO
                            agendamentosDAO.CadastrarAgendamento(codcliente, codbarbeiro, codservico, valor, data, hora, observacao);

                            //PASSANDO O CODCLIENTE PARA O MÉTODO EM DAO PARA RETORNAR O EMAIL DO CLIENTE
                            String emailCliente = clientesDAO.RetornarEmailPorCodCliente(codcliente);

                            //PASSANDO O CODBARBEIRO PARA O MÉTODO EM DAO PARA RETORNAR O EMAIL DO BARBEIRO
                            String emailBarbeiro = barbeirosDAO.RetornarEmailPorCodBarbeiro(codbarbeiro);

                            //PASSANDO EMAIL DA BARBEARIA
                            String emailBarbearia = "rhz.sistemas.projeto@gmail.com";

                            //INFORMAÇÕES DO DO AGENDAMENTO - CLIENTE
                            Email envioEmailCliente = new Email("Agendamento Barbearia RHZ",//ASSUNTO
                                    "Segue as informações referentes a seu agendamento - Cliente: \n" //CONTEÚDO
                                    + "\nData: " + dataConvertida + "\nHora: " + hora + "\nBarbeiro: " + barbeiroString + "\nServiço: " + servicoString + "", //CONTEÚDO
                                    emailCliente); //DESTINATÁRIO

                            //INFORMAÇÕES DO DO AGENDAMENTO - BARBEIRO
                            Email envioEmailBarbeiro = new Email("Agendamento Barbearia RHZ",//ASSUNTO
                                    "Segue as informações referentes a seu agendamento - Barbeiro: \n" //CONTEÚDO
                                    + "\nData: " + dataConvertida + "\nHora: " + hora + "\nCliente: " + clienteString + "\nServiço: " + servicoString + "", //CONTEÚDO
                                    emailBarbeiro); //DESTINATÁRIO

                            //INFORMAÇÕES DO EMAIL DO AGENDAMENTO - BARBEARIA
                            Email envioEmailBarbearia = new Email("Agendamento Barbearia RHZ",//ASSUNTO
                                    "Segue as informações referentes a seu agendamento - Barbearia: \n" //CONTEÚDO
                                    + "\nData: " + dataConvertida + "\nHora: " + hora + "\nBarbeiro: " + barbeiroString + "\nCliente: " + clienteString + "\nServiço: " + servicoString + "", //CONTEÚDO
                                    emailBarbearia);//DESTINATÁRIO

                            //ENVIANDO OS EMAILS
                            envioEmailCliente.EnviarEmail();
                            envioEmailBarbeiro.EnviarEmail();
                            envioEmailBarbearia.EnviarEmail();

                        } else {
                            //hora
                            JOptionPane.showMessageDialog(null, "Campo de hora não pode estar vazio!");

                        }

                    } else {
                        //data
                        JOptionPane.showMessageDialog(null, "Campo de data não pode estar vazio!");

                    }

                } else {
                    //servicos
                    JOptionPane.showMessageDialog(null, "Campo de serviços não pode estar vazio!");

                }

            } else {
                //barbeiros
                JOptionPane.showMessageDialog(null, "Campo de barbeiros não pode estar vazio!");

            }

        } else {
            //clientes
            JOptionPane.showMessageDialog(null, "Campo de clientes não pode estar vazio!");

        }

    }

    public void CarregarTabelaAgendamentos() {

        DefaultTableModel model = (DefaultTableModel) view.getjTableAgendamentos().getModel();
        model.setNumRows(0);

        for (Agendamentos a : agendamentosDAO.ListarAgendamentosComInnerJoin()) {

            model.addRow(new Object[]{
                a.getCodagendamento(),
                /*INNER JOIN*/ a.getClienteString(),
                /*INNER JOIN*/ a.getBarbeiroString(),
                /*INNER JOIN*/ a.getServicoString(),
                a.getValor(),
                Data.Format(a.getData()),
                a.getHora(),
                a.getObservacao()

            });
        }

    }

    public void CarregarTabelaAgendamentosPorCliente(String clientepesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableAgendamentos().getModel();
        model.setNumRows(0);

        for (Agendamentos a : agendamentosDAO.ListarAgendamentosPorClienteComInnerJoin(clientepesquisa)) {

            model.addRow(new Object[]{
                a.getCodagendamento(),
                /*INNER JOIN*/ a.getClienteString(),
                /*INNER JOIN*/ a.getBarbeiroString(),
                /*INNER JOIN*/ a.getServicoString(),
                a.getValor(),
                Data.Format(a.getData()),
                a.getHora(),
                a.getObservacao()

            });
        }

    }

    public void CarregarTabelaAgendamentosPorBarbeiro(String barbeiroPesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableAgendamentos().getModel();
        model.setNumRows(0);

        for (Agendamentos a : agendamentosDAO.ListarAgendamentosPorBarbeiroComInnerJoin(barbeiroPesquisa)) {

            model.addRow(new Object[]{
                a.getCodagendamento(),
                /*INNER JOIN*/ a.getClienteString(),
                /*INNER JOIN*/ a.getBarbeiroString(),
                /*INNER JOIN*/ a.getServicoString(),
                a.getValor(),
                Data.Format(a.getData()),
                a.getHora(),
                a.getObservacao()

            });
        }

    }

    public void CarregarTabelaAgendamentosPorServico(String servicoPesquisa) {

        DefaultTableModel model = (DefaultTableModel) view.getjTableAgendamentos().getModel();
        model.setNumRows(0);

        for (Agendamentos a : agendamentosDAO.ListarAgendamentosPorServicoComInnerJoin(servicoPesquisa)) {

            model.addRow(new Object[]{
                a.getCodagendamento(),
                /*INNER JOIN*/ a.getClienteString(),
                /*INNER JOIN*/ a.getBarbeiroString(),
                /*INNER JOIN*/ a.getServicoString(),
                a.getValor(),
                Data.Format(a.getData()),
                a.getHora(),
                a.getObservacao()

            });
        }

    }

    public void LimparCampos() {

        view.getjComboBoxCliente().setSelectedIndex(0);
        view.getjComboBoxBarbeiro().setSelectedIndex(0);
        view.getjComboBoxServico().setSelectedIndex(0);
        view.getjTextFieldValor().setText("");
        view.getjDateChooserAgendamentos().setDate(null);
        view.getjFormattedTextFieldHora().setText("");
        view.getjTextAreaObservacao().setText("");

    }

    public void BotaoPesquisar() {

        if (view.getjComboBoxPesquisa().getSelectedIndex() == 0) { //cliente

            PesquisarAgendamentoPorCliente();

        }

        if (view.getjComboBoxPesquisa().getSelectedIndex() == 1) { //barbeiro

            PesquisarAgendamentoPorBarbeiro();

        }

        if (view.getjComboBoxPesquisa().getSelectedIndex() == 2) { //serviço

            PesquisarAgendamentoPorServico();

        }

    }

    public void BotaoConcluir() {

        ConcluirAgendamento();
        CarregarTabelaAgendamentos();

    }

    public void BotaoExcluir() {

        ExcluirAgendamento();
        CarregarTabelaAgendamentos();

    }

    public void BotaoSalvar() {

        AtualizarAgendamento();
        CarregarTabelaAgendamentos();
        LimparCampos();

    }

    public void BotaoLimpar() {

        LimparCampos();

    }

    public void BotaoNovo() {

        CadastrarAgendamento();
        CarregarTabelaAgendamentos();
        LimparCampos();

    }

}
