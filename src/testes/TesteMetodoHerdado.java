package testes;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class TesteMetodoHerdado {

    public void MetodoLogger(String metodo, String classe, Object erro) {

        try {

            Logger logger = Logger.getLogger(TesteMetodoHerdado.class.getName());

            FileHandler fh = null;

            //just to make our log file nicer :)
            SimpleDateFormat format = new SimpleDateFormat("M-d_HHmmss");

            fh = new FileHandler("C:/temp/test/MyLogFile_"
                    + format.format(Calendar.getInstance().getTime()) + ".log");

            fh.setFormatter(new SimpleFormatter());
            logger.addHandler(fh);

            logger.info("Erro na classe "+classe+"  - Método "+metodo+"");
            logger.info("Erro na classe x - Método y");
            logger.info(erro.toString());

        } catch (IOException | SecurityException ex) {
            
//            e.printStackTrace();
             Logger.getLogger(TesteMetodoHerdado.class.getName()).log(Level.SEVERE, null, ex);
             
            
        }

//        logger.severe("error message");
//        logger.fine("fine message"); //won't show because to high level of logging
    }
    
}
