package model;

import java.util.Date;

public class Agendamentos {
    
    int codagendamento;
    int cliente;
    int barbeiro;
    int servico;
    /*INNER JOIN*/ String clienteString;
    /*INNER JOIN*/ String barbeiroString;
    /*INNER JOIN*/ String servicoString;
    double valor;
    Date data;
    String hora;
    String observacao;

    public int getCodagendamento() {
        return codagendamento;
    }

    public void setCodagendamento(int codagendamento) {
        this.codagendamento = codagendamento;
    }

    public int getCliente() {
        return cliente;
    }

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getBarbeiro() {
        return barbeiro;
    }

    public void setBarbeiro(int barbeiro) {
        this.barbeiro = barbeiro;
    }

    public int getServico() {
        return servico;
    }

    public void setServico(int servico) {
        this.servico = servico;
    }

    public String getClienteString() {
        return clienteString;
    }

    public void setClienteString(String clienteString) {
        this.clienteString = clienteString;
    }

    public String getBarbeiroString() {
        return barbeiroString;
    }

    public void setBarbeiroString(String barbeiroString) {
        this.barbeiroString = barbeiroString;
    }

    public String getServicoString() {
        return servicoString;
    }

    public void setServicoString(String servicoString) {
        this.servicoString = servicoString;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
    
}
