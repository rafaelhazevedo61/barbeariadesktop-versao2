package dao;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Clientes;
import util.Data;
import util.FabricaConexao;

public class ClientesDAO {

    public String RetornarEmailPorCodCliente(int codcliente) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        ResultSet rs = null;

        try {

            String sql = "select email from clientes where codcliente = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codcliente);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getString("email");

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornaEmailPorCodCliente() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }
        
        return "";

    }

    public void ExcluirCliente(int codcliente) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "delete from clientes where codcliente = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codcliente);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Cliente excluído com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarClientes() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public Iterable<Clientes> ListarClientesPorNome(String nome) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        ResultSet rs = null;

        List<Clientes> clientes = new ArrayList<>();

        String sql = "SELECT codcliente, nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email\n"
                + "	FROM clientes "
                + "WHERE nome like ?";

        try {

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%" + nome + "%");

            rs = pst.executeQuery();

            while (rs.next()) {

                clientes.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarClientes() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return clientes;

    }

    public Iterable<Clientes> ListarClientes() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        ResultSet rs = null;

        List<Clientes> clientes = new ArrayList<>();

        String sql = "SELECT codcliente, nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email\n"
                + "	FROM clientes;";

        try {

            pst = conexao.prepareStatement(sql);

            rs = pst.executeQuery();

            while (rs.next()) {

                clientes.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarClientes() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return clientes;

    }

    public void AtualizarCliente(int codcliente, String nome, String cpf, String email, Date dataNascimento, int sexo, String cep, String rua, String numero, String complemento, String bairro,
            String contato1, String contato2, boolean recebe_email) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "UPDATE clientes\n"
                    + "	SET nome=?, cpf=?, email=?, data_nascimento=?, sexo=?, cep=?, rua=?, numero=?, "
                    + "complemento=?, bairro=?, contato1=?, contato2=?, recebe_email=?\n"
                    + "	WHERE codcliente=?;";

            pst = conexao.prepareStatement(sql);

            pst.setString(1, nome.toUpperCase());
            pst.setString(2, cpf);
            pst.setString(3, email);
            pst.setDate(4, Data.ConvertDataFormParaBanco(dataNascimento));
            pst.setInt(5, sexo);
            pst.setString(6, cep);
            pst.setString(7, rua.toUpperCase());
            pst.setString(8, numero.toUpperCase());
            pst.setString(9, complemento.toUpperCase());
            pst.setString(10, bairro.toUpperCase());
            pst.setString(11, contato1);
            pst.setString(12, contato2);
            pst.setBoolean(13, recebe_email);
            pst.setInt(14, codcliente);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Cliente atualizado com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarCliente() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void CadastrarCliente(String nome, String cpf, String email, Date dataNascimento, int sexo, String cep, String rua, String numero, String complemento, String bairro,
            String contato1, String contato2, boolean recebe_email) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "INSERT INTO clientes(\n"
                    + "	nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email)\n"
                    + "	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            pst = conexao.prepareStatement(sql);

            pst.setString(1, nome.toUpperCase());
            pst.setString(2, cpf);
            pst.setString(3, email);
            pst.setDate(4, Data.ConvertDataFormParaBanco(dataNascimento));
            pst.setInt(5, sexo);
            pst.setString(6, cep);
            pst.setString(7, rua.toUpperCase());
            pst.setString(8, numero.toUpperCase());
            pst.setString(9, complemento.toUpperCase());
            pst.setString(10, bairro.toUpperCase());
            pst.setString(11, contato1);
            pst.setString(12, contato2);
            pst.setBoolean(13, recebe_email);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método CadastrarCliente() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public String RetornaClientePorCodCliente(int codcliente) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        try {

            String sql = "select nome from clientes where codcliente = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codcliente);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getString("nome");

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornaClientePorCodCliente() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return "";

    }

    public int RetornaCodclientePorClientes(String cliente) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        try {

            String sql = "select codcliente from clientes where nome = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, cliente);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getInt("codcliente");

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornaCodclientesPorClientes() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return 0;

    }

    public Iterable<Clientes> ListarClientesJComboBox() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Clientes> clientes = new ArrayList();

        try {

            String sql = "select nome from clientes order by nome";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                clientes.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(UsuariosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarClientesJComboBox() na classe ClientesDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return clientes;

    }

    public Clientes CarregarResultSet1(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO TODOS OS CAMPOS
        Clientes cliente = new Clientes();

        cliente.setCodcliente(rs.getInt("codcliente"));
        cliente.setNome(rs.getString("nome"));
        cliente.setCpf(rs.getString("cpf"));
        cliente.setEmail(rs.getString("email"));
        cliente.setData_nascimento(rs.getDate("data_nascimento"));
        cliente.setSexo(rs.getInt("sexo"));
        cliente.setCep(rs.getString("cep"));
        cliente.setRua(rs.getString("rua"));
        cliente.setNumero(rs.getString("numero"));
        cliente.setComplemento(rs.getString("complemento"));
        cliente.setBairro(rs.getString("bairro"));
        cliente.setContato1(rs.getString("contato1"));
        cliente.setContato2(rs.getString("contato2"));
        cliente.setRecebe_email(rs.getBoolean("recebe_email"));

        return cliente;

    }

    public Clientes CarregarResultSet2(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO O CAMPO NOME
        Clientes cliente = new Clientes();

        cliente.setNome(rs.getString("nome"));

        return cliente;

    }

}
