package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Barbeiros;
import util.Data;
import util.FabricaConexao;

public class BarbeirosDAO {
    
    public String RetornarEmailPorCodBarbeiro(int codbarbeiro){
        
        Connection conexao = FabricaConexao.abrirConexao();
        
        PreparedStatement pst = null;
        
        ResultSet rs = null;
        
        try {
            
            String sql = "select email from barbeiros where codbarbeiro = ?";
            
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codbarbeiro);
            
            pst.execute();
            
            rs = pst.getResultSet();
            
            if(rs.next()){
                
                return rs.getString("email");
                
            }
            
        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornarEmailPorCodBarbeiro() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }
        
        return "";
        
    }

    public void ExcluirBarbeiro(int codbarbeiro) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "delete from barbeiros where codbarbeiro = ?";

            pst = conexao.prepareStatement(sql);

            pst.setInt(1, codbarbeiro);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Barbeiro excluído com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarBarbeiro() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void AtualizarBarbeiro(int codbarbeiro, String nome, String cpf, String email, int sexo, Date dataNascimento, String cep, String rua, String numero, String complemento, String bairro,
            String contato1, String contato2, boolean recebe_email) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "UPDATE barbeiros\n"
                    + "	SET nome=?, cpf=?, email=?, data_nascimento=?, sexo=?, cep=?, rua=?, numero=?, "
                    + "complemento=?, bairro=?, contato1=?, contato2=?, recebe_email=?\n"
                    + "	WHERE codbarbeiro=?;";

            pst = conexao.prepareStatement(sql);

            pst.setString(1, nome.toUpperCase());
            pst.setString(2, cpf);
            pst.setString(3, email);
            pst.setDate(4, Data.ConvertDataFormParaBanco(dataNascimento));
            pst.setInt(5, sexo);
            pst.setString(6, cep);
            pst.setString(7, rua.toUpperCase());
            pst.setString(8, numero.toUpperCase());
            pst.setString(9, complemento.toUpperCase());
            pst.setString(10, bairro.toUpperCase());
            pst.setString(11, contato1);
            pst.setString(12, contato2);
            pst.setBoolean(13, recebe_email);
            pst.setInt(14, codbarbeiro);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Barbeiro atualizado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarBarbeiro() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void CadastrarBarbeiro(String nome, String cpf, String email, int sexo, Date dataNascimento, String cep, String rua, String numero, String complemento, String bairro,
            String contato1, String contato2, boolean recebe_email) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "INSERT INTO barbeiros(\n"
                    + "	nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email)\n"
                    + "	VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

            pst = conexao.prepareStatement(sql);

            pst.setString(1, nome.toUpperCase());
            pst.setString(2, cpf);
            pst.setString(3, email);
            pst.setDate(4, Data.ConvertDataFormParaBanco(dataNascimento));
            pst.setInt(5, sexo);
            pst.setString(6, cep);
            pst.setString(7, rua.toUpperCase());
            pst.setString(8, numero.toUpperCase());
            pst.setString(9, complemento.toUpperCase());
            pst.setString(10, bairro.toUpperCase());
            pst.setString(11, contato1);
            pst.setString(12, contato2);
            pst.setBoolean(13, recebe_email);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Barbeiro cadastrado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método CadastrarBarbeiro() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public int RetornaCodbarbeiroPorBarbeiros(String barbeiro) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        try {

            String sql = "select codbarbeiro from barbeiros where nome = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, barbeiro);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getInt("codbarbeiro");

            }

        } catch (SQLException ex) {

            Logger.getLogger(BarbeirosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornaCodbarbeiroPorBarbeiros() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return 0;

    }

    public Iterable<Barbeiros> ListarBarbeiros() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Barbeiros> barbeiros = new ArrayList<>();

        try {

            String sql = "SELECT codbarbeiro, nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email\n"
                    + "	FROM barbeiros;";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                barbeiros.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarBarbeiros() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return barbeiros;

    }

    public Iterable<Barbeiros> ListarBarbeirosPorNome(String nome) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Barbeiros> barbeiros = new ArrayList<>();

        try {

            String sql = "SELECT codbarbeiro, nome, cpf, email, data_nascimento, sexo, cep, rua, numero, complemento, bairro, contato1, contato2, recebe_email\n"
                    + "	FROM barbeiros "
                    + "WHERE nome like ?";

            pst = conexao.prepareStatement(sql);

            pst.setString(1, "%" + nome + "%");

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                barbeiros.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarBarbeirosPorNome() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return barbeiros;

    }

    public Iterable<Barbeiros> ListarBarbeirosJComboBox() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Barbeiros> barbeiros = new ArrayList();

        try {

            String sql = "select nome from barbeiros order by nome";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                barbeiros.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(BarbeirosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarBarbeirosJComboBox() na classe BarbeirosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return barbeiros;

    }

    public Barbeiros CarregarResultSet1(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO TODOS OS CAMPOS
        Barbeiros barbeiro = new Barbeiros();

        barbeiro.setCodbarbeiro(rs.getInt("codbarbeiro"));
        barbeiro.setNome(rs.getString("nome"));
        barbeiro.setCpf(rs.getString("cpf"));
        barbeiro.setEmail(rs.getString("email"));
        barbeiro.setData_nascimento(rs.getDate("data_nascimento"));
        barbeiro.setSexo(rs.getInt("sexo"));
        barbeiro.setCep(rs.getString("cep"));
        barbeiro.setRua(rs.getString("rua"));
        barbeiro.setNumero(rs.getString("numero"));
        barbeiro.setComplemento(rs.getString("complemento"));
        barbeiro.setBairro(rs.getString("bairro"));
        barbeiro.setContato1(rs.getString("contato1"));
        barbeiro.setContato2(rs.getString("contato2"));
        barbeiro.setRecebe_email(rs.getBoolean("recebe_email"));

        return barbeiro;

    }

    public Barbeiros CarregarResultSet2(ResultSet rs) throws SQLException {

        //RESULT SET CONTENDO O CAMPO NOME
        Barbeiros barbeiro = new Barbeiros();

        barbeiro.setNome(rs.getString("nome"));

        return barbeiro;

    }

}
