package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Servicos;
import util.FabricaConexao;

public class ServicosDAO {

    public void AtualizarServico(int codservico, String nomeservico, double valorservico, String observacao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "UPDATE servicos "
                    + "SET nome = ?, valor = ?, observacao = ? "
                    + "WHERE codservico = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, nomeservico);
            pst.setDouble(2, valorservico);
            pst.setString(3, observacao);
            pst.setInt(4, codservico);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Serviço atualizado com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarServico() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void ExcluirServico(int codservico) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "delete from servicos where codservico = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codservico);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Serviço excluído com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ExcluirServico() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void CadastrarServico(String nomeservico, double valorservico, String observacao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "INSERT INTO servicos(\n"
                    + "	nome, valor, observacao)\n"
                    + "	VALUES (?, ?, ?);";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, nomeservico);
            pst.setDouble(2, valorservico);
            pst.setString(3, observacao);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Serviço cadastrado com sucesso!");
            JOptionPane.showMessageDialog(null, "Por favor reiniciar a aplicação!");

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método CadastrarServico() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public int RetornaCodservicoPorServicos(String servico) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        try {

            String sql = "select codservico from servicos where nome = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, servico);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getInt("codservico");

            }

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornaCodservicoPorServicos() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return 0;

    }

    public double RetornarValorPorServico(String servico) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        try {

            String sql = "select valor from servicos where nome = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, servico);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next()) {

                return rs.getDouble("valor");

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornarValorPorServico() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return 0;

    }

    public Iterable<Servicos> ListarServicos() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Servicos> servicos = new ArrayList<>();

        try {

            String sql = "SELECT codservico, nome, valor, observacao\n"
                    + "	FROM servicos;";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                servicos.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarServicos() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return servicos;
    }

    public Iterable<Servicos> ListarServicosPorServico(String servico) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Servicos> servicos = new ArrayList<>();

        try {

            String sql = "select codservico, nome, valor, observacao "
                    + "from servicos "
                    + "where nome like ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%"+servico+"%");

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                servicos.add(CarregarResultSet1(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarServicosPorServico() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return servicos;
    }

    public Iterable<Servicos> ListarServicosJComboBox() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Servicos> servicos = new ArrayList<>();

        try {

            String sql = "select nome from servicos order by nome";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                servicos.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarServicosJComboBox() na classe ServicosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return servicos;
    }

    public Servicos CarregarResultSet1(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO TODOS OS CAMPOS
        Servicos servico = new Servicos();

        servico.setCodservico(rs.getInt("codservico"));
        servico.setNome(rs.getString("nome"));
        servico.setValor(rs.getDouble("valor"));
        servico.setObservacao(rs.getString("observacao"));

        return servico;

    }

    public Servicos CarregarResultSet2(ResultSet rs) throws SQLException {

        //RESULT SET CONTENDO CAMPO NOME
        Servicos servico = new Servicos();

        servico.setNome(rs.getString("nome"));

        return servico;

    }

}
