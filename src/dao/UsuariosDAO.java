package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Usuarios;
import testes.TesteMetodoHerdado;
import util.FabricaConexao;

public class UsuariosDAO {

    public void ExcluirUsuario(int codusuario) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "delete from usuarios where codusuario = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codusuario);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Usuário excluído com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ExcluirUsuario() na classe UsuariosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void AtualizarUsuario(int codusuario, String usuario, String senha, int permissao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "UPDATE usuarios "
                    + "SET usuario = ?, senha = ?, permissao = ? "
                    + "WHERE codusuario = ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario);
            pst.setString(2, senha);
            pst.setInt(3, permissao);
            pst.setInt(4, codusuario);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Usuário atualizado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarUsuario() na classe UsuariosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public Iterable<Usuarios> ListarUsuariosPorUsuarioComInnerJoin(String usuario) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Usuarios> usuarios = new ArrayList<>();

        try {

        String sql = "select codusuario, usuario, senha, usuarios_permissao.descricao as permissaousuario \n"
                + "from usuarios\n"
                + "inner join usuarios_permissao on usuarios_permissao.codpermissao = usuarios.permissao\n"
                + "where usuario like ?";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%" + usuario + "%");

            rs = pst.executeQuery();

            while (rs.next()) {

                usuarios.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarUsuariosPorUsuarioComInnerJoin() na classe UsuariosDao");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return usuarios;

    }

    public Iterable<Usuarios> ListarUsuariosComInnerJoin() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Usuarios> usuarios = new ArrayList<>();

        try {

            String sql = "select codusuario, usuario, senha, usuarios_permissao.descricao as permissaousuario\n"
                    + "from usuarios\n"
                    + "inner join usuarios_permissao on usuarios_permissao.codpermissao = usuarios.permissao";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                usuarios.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarUsuariosComInnerJoin() na classe UsuariosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return usuarios;

    }

    public void CadastrarUsuario(String usuario, String senha, int permissao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "INSERT INTO usuarios(\n"
                    + "	usuario, senha, permissao)\n"
                    + "	VALUES (?, ?, ?);";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario);
            pst.setString(2, senha);
            pst.setInt(3, permissao);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Usuário cadastrado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método CadastrarUsuario() na classe UsuariosDAO");

            String metodo = "ListarUsuariosComInnerjoin";
            String classe = this.getClass().toString();

            TesteMetodoHerdado teste = new TesteMetodoHerdado();
            teste.MetodoLogger(metodo, classe, ex);

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public boolean existeUsuario(String usuario, String senha) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        boolean retorno = false;

        try {

            String sql = "select * "
                    + "from usuarios "
                    + "where "
                    + "usuario = ? "
                    + "and senha = ? ";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario);
            pst.setString(2, senha);

            pst.execute();

            rs = pst.getResultSet();

            if (rs.next() == true) {

                retorno = true;

            } else {

                retorno = false;

            }

        } catch (SQLException ex) {

            Logger.getLogger(UsuariosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método existeUsuario() na classe UsuariosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return retorno;

    }

    public Usuarios CarregarResultSet2(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO TODOS OS CAMPOS - USANDO INNERJOIN
        Usuarios usuario = new Usuarios();

        usuario.setCodusuario(rs.getInt("codusuario"));
        usuario.setUsuario(rs.getString("usuario"));
        usuario.setSenha(rs.getString("senha"));
        usuario.setPermissaoString(rs.getString("permissaousuario"));

        return usuario;

    }

}
