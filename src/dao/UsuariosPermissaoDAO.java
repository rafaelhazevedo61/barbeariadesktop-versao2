/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.UsuariosPermissao;
import util.FabricaConexao;

/**
 *
 * @author Rafael
 */
public class UsuariosPermissaoDAO {
    
    public int RetornarCodPermissaoPorPermissao (String permissao) {
        
        Connection conexao = FabricaConexao.abrirConexao();
        
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        try {
            
            String sql = "select codpermissao from usuarios_permissao where descricao = ?";
            
            pst = conexao.prepareStatement(sql);
            pst.setString(1, permissao);
            
            pst.execute();
            
            rs = pst.getResultSet();
            
            if(rs.next()){
                
                return rs.getInt("codpermissao");
                
            }
            
        } catch (SQLException ex) {

            Logger.getLogger(ServicosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método RetornarCodPermissaoPorPermissao() na classe UsuariosPermissaoDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return 0;
        
    }
    
    public Iterable<UsuariosPermissao> ListarPermissoesJComboBox(){
        
        Connection conexao = FabricaConexao.abrirConexao();
        
        PreparedStatement pst = null;
        ResultSet rs = null;
        
        List<UsuariosPermissao> permissoes = new ArrayList<>();
        
        try {
            
            String sql = "SELECT descricao\n" +
        "	FROM usuarios_permissao;";
            
            pst = conexao.prepareStatement(sql);
            
            pst.executeQuery();
            
            rs = pst.getResultSet();
            
            while (rs.next()){
                
                permissoes.add(CarregarResultSet1(rs));
                
            }
            
        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarPermissoesJComboBox() na classe UsuariosPermissaoDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return permissoes;
        
    }
    
    public UsuariosPermissao CarregarResultSet1 (ResultSet rs) throws SQLException {
        
        //RESULTSET CONTENDO O CAMPO NOME
        
        UsuariosPermissao permissao = new UsuariosPermissao();
        
        permissao.setDescricao(rs.getString("descricao"));
        
        return permissao;
        
    }
    
}
