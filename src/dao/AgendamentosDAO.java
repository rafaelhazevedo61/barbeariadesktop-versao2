package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Agendamentos;
import util.Data;
import util.FabricaConexao;

public class AgendamentosDAO {

    public void ConcluirAgendamento(int codagendamento) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "update agendamentos\n"
                    + "	set status = 1 \n"
                    + "	where codagendamento = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codagendamento);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Agendamento concluído com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ConcluirAgendamento() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void ExcluirAgendamento(int codagendamento) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "update agendamentos\n"
                    + "	set status = 2 \n"
                    + "	where codagendamento = ?";

            pst = conexao.prepareStatement(sql);
            pst.setInt(1, codagendamento);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Agendamento excluído com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ExcluirAgendamento() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void AtualizarAgendamento(int codagendamento, int codcliente, int codbarbeiro, int codservico, double valor, Date data, String hora, String observacao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "UPDATE agendamentos\n"
                    + "	SET cliente=?, barbeiro=?, servico=?, valor=?, data=?, hora=?, observacao=?\n"
                    + "	WHERE codagendamento=?;";

            pst = conexao.prepareStatement(sql);

            pst.setInt(1, codcliente);
            pst.setInt(2, codbarbeiro);
            pst.setInt(3, codservico);
            pst.setDouble(4, valor);
            pst.setDate(5, Data.ConvertDataFormParaBanco(data));
            pst.setString(6, hora);
            pst.setString(7, observacao);
            pst.setInt(8, codagendamento);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Agendamento atualizado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método AtualizarAgendamento() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public void CadastrarAgendamento(int codcliente, int codbarbeiro, int codservico, double valor, Date data, String hora, String observacao) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;

        try {

            String sql = "insert into agendamentos "
                    + "(cliente, barbeiro, servico, valor, data, hora, observacao)"
                    + "values(?,?,?,?,?,?,?)";

            pst = conexao.prepareStatement(sql);

            pst.setInt(1, codcliente);
            pst.setInt(2, codbarbeiro);
            pst.setInt(3, codservico);
            pst.setDouble(4, valor);
            pst.setDate(5, Data.ConvertDataFormParaBanco(data));
            pst.setString(6, hora);
            pst.setString(7, observacao);

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Agendamento cadastrado com sucesso!");

        } catch (SQLException ex) {

            Logger.getLogger(ClientesDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método CadastrarAgendamento() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

    }

    public Iterable<Agendamentos> ListarAgendamentosComInnerJoin() {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Agendamentos> agendamentos = new ArrayList<>();

        try {

            String sql = "select codagendamento, clientes.nome as nomecliente, barbeiros.nome as nomebarbeiro, servicos.nome as nomeservico, agendamentos.valor, \n"
                    + "data, hora, agendamentos.observacao\n"
                    + "from agendamentos\n"
                    + "inner join clientes on clientes.codcliente = agendamentos.cliente\n"
                    + "inner join barbeiros on barbeiros.codbarbeiro = agendamentos.barbeiro\n"
                    + "inner join servicos on servicos.codservico = agendamentos.servico\n"
                    + "where agendamentos.status = 0\n"
                    + "order by codagendamento";

            pst = conexao.prepareStatement(sql);

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                agendamentos.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarAgendamentosComInnerJoin() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return agendamentos;

    }

    public Iterable<Agendamentos> ListarAgendamentosPorClienteComInnerJoin(String cliente) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Agendamentos> agendamentos = new ArrayList<>();

        try {

            String sql = "select codagendamento, clientes.nome as nomecliente, barbeiros.nome as nomebarbeiro, servicos.nome as nomeservico, agendamentos.valor, data, hora, agendamentos.observacao\n"
                    + "from agendamentos\n"
                    + "inner join clientes on clientes.codcliente = agendamentos.cliente\n"
                    + "inner join barbeiros on barbeiros.codbarbeiro = agendamentos.barbeiro\n"
                    + "inner join servicos on servicos.codservico = agendamentos.servico\n"
                    + "where clientes.nome like ?"
                    + "and agendamentos.status = 0\n"
                    + "order by data desc";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%" + cliente + "%");

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                agendamentos.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarAgendamentosPorClienteComInnerJoin() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return agendamentos;

    }

    public Iterable<Agendamentos> ListarAgendamentosPorBarbeiroComInnerJoin(String barbeiro) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Agendamentos> agendamentos = new ArrayList<>();

        try {

            String sql = "select codagendamento, clientes.nome as nomecliente, barbeiros.nome as nomebarbeiro, servicos.nome as nomeservico, agendamentos.valor, data, hora, agendamentos.observacao\n"
                    + "from agendamentos\n"
                    + "inner join clientes on clientes.codcliente = agendamentos.cliente\n"
                    + "inner join barbeiros on barbeiros.codbarbeiro = agendamentos.barbeiro\n"
                    + "inner join servicos on servicos.codservico = agendamentos.servico\n"
                    + "where barbeiros.nome like ?"
                    + "and agendamentos.status = 0\n"
                    + "order by data desc";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%" + barbeiro + "%");

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                agendamentos.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarAgendamentosPorBarbeiroComInnerJoin() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return agendamentos;

    }

    public Iterable<Agendamentos> ListarAgendamentosPorServicoComInnerJoin(String servico) {

        Connection conexao = FabricaConexao.abrirConexao();

        PreparedStatement pst = null;
        ResultSet rs = null;

        List<Agendamentos> agendamentos = new ArrayList<>();

        try {

            String sql = "select codagendamento, clientes.nome as nomecliente, barbeiros.nome as nomebarbeiro, servicos.nome as nomeservico, agendamentos.valor, data, hora, agendamentos.observacao\n"
                    + "from agendamentos\n"
                    + "inner join clientes on clientes.codcliente = agendamentos.cliente\n"
                    + "inner join barbeiros on barbeiros.codbarbeiro = agendamentos.barbeiro\n"
                    + "inner join servicos on servicos.codservico = agendamentos.servico\n"
                    + "where servicos.nome like ?"
                    + "and agendamentos.status = 0\n"
                    + "order by data desc";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, "%" + servico + "%");

            pst.executeQuery();

            rs = pst.getResultSet();

            while (rs.next()) {

                agendamentos.add(CarregarResultSet2(rs));

            }

        } catch (SQLException ex) {

            Logger.getLogger(AgendamentosDAO.class.getName()).log(Level.SEVERE, null, ex);

            System.out.println("Erro no método ListarAgendamentosPorServicoComInnerJoin() na classe AgendamentosDAO");

        } finally {

            FabricaConexao.fecharConexao();

        }

        return agendamentos;

    }

    public Agendamentos CarregarResultSet2(ResultSet rs) throws SQLException {

        //RESULTSET CONTENDO TODOS OS CAMPOS - USANDO INNER JOIN
        Agendamentos agendamento = new Agendamentos();

        agendamento.setCodagendamento(rs.getInt("codagendamento"));
        agendamento.setClienteString(rs.getString("nomecliente"));
        agendamento.setBarbeiroString(rs.getString("nomebarbeiro"));
        agendamento.setServicoString(rs.getString("nomeservico"));
        agendamento.setValor(rs.getDouble("valor"));
        agendamento.setData(rs.getDate("data"));
        agendamento.setHora(rs.getString("hora"));
        agendamento.setObservacao(rs.getString("observacao"));

        return agendamento;

    }

}
